import React from 'react';

export default function IEWarning() {
  return (
    <script
      type="text/javascript"
      dangerouslySetInnerHTML={{
        __html: `
      ;(function() {
        var __is_IE = /*@cc_on!@*/false || !!document.documentMode;
        // Hack to detect IE 6-11
        if( __is_IE && !localStorage.getItem('ie_warning_ignored') ) {
          
          document.write(
            '<div class="__ie_warning" id="__ie_warning">' +
            '  <div class="__ie_warning-pad" role="status" aria-live="polite">' +
            '    <b class="__ie_warning-mainmsg">Tu navegador está desactualizado.</b>' +
            '    <span class="__ie_warning-moremsg">Muchas páginas no van a funcionar correctamente hasta que lo actualices. <br/>Instala otro navegador para tener una mejor experiencia de uso.</span>' +
            '    <span class="__ie_warning-buttons">' +
            '      <a id="__ie_warning-cta" href="https://www.google.com/chrome/" target="_blank" rel="noopener noreferrer">Instalar Chrome</a>' +
            '      <a id="__ie_warning-ignore" role="button" href="#">Ignorar</a>' +
            '    </span>' +
            '  </div>' +
            '  <style>' +
            '    .__ie_warning {position:absolute;position:fixed;z-index:111111; width:100%; top:0px; left:0px; border-bottom:1px solid #A29330; text-align:center; color:#000; background-color: #fff8ea; font: 16px Calibri,Helvetica,sans-serif;}' +
            '    .__ie_warning-pad { padding: 9px;  line-height: 1.7em; }' +
            '    .__ie_warning-buttons { display: block; text-align: center; margin-top: 0.5em; }' +
            '    #__ie_warning-ignore, #__ie_warning-cta, #__ie_warningpermanent { color: #fff; text-decoration: none; cursor:pointer; padding: 1px 12px 2px 12px; border: 0px; border-radius: 4px; font-weight: bold; background: #5ab400; white-space: nowrap; margin: 0 2px; display: inline-block;}' +
            '    #__ie_warning-ignore { background-color: gray; font-weight: normal; }' +
            '    @media only screen and (max-width: 700px){' +
            '      .__ie_warning div { padding:5px 12px 5px 9px; line-height: 1.3em;}' +
            '    }' +
            '  </style>' +
            '</div>'                    
          );

          document.getElementById('__ie_warning-ignore').addEventListener('click', function(event) {
            event.preventDefault();
            localStorage.setItem('ie_warning_ignored', "" + Date.now());
            var $warning = document.querySelector('#__ie_warning');
            $warning.parentNode.removeChild($warning);
          });
        }
      })();
    `,
      }}
    />
  );
}
