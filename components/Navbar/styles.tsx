import styled from '@emotion/styled';
import { theme } from '../../styles/theme';

export const Wrapper = styled.nav`
  height: 50px;
  margin-bottom: ${theme.spacers.lg};
  background: ${theme.colors.light};
`;

export const Logo = styled.div`
  width: 75px;
  height: 30px;
  background: ${theme.colors.dark};
`;

export const Flex = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`;

export const Icon = styled.div`
  margin-right: ${theme.spacers.sm};
`;
export const Brand = styled.div`
  display: flex;
  align-items: center;
`;
export const Menu = styled.ul`
  list-style: none;

`;

export const MenuItem = styled.li`
  display: inline-block;
  a {
    ${theme.fonts.link}
  }
`;
