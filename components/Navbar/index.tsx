import React, { FunctionComponent } from 'react';
import Link from 'next/link';

import { Wrapper, MenuItem, Menu, Logo, Icon, Brand, Flex } from './styles';
import { Container } from '../../styles/grid';

const Navbar = () => {
  return (
    <Wrapper>
      <Container className="container">
        <Flex>
          <Brand>
            <Icon>
              <svg width="24" height="15" viewBox="0 0 24 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="24" height="3" fill="#C4C4C4" />
                <rect y="6" width="24" height="3" fill="#C4C4C4" />
                <rect y="12" width="24" height="3" fill="#C4C4C4" />
              </svg>
            </Icon>
            <Logo />
          </Brand>
          <Menu>
            <MenuItem>
              <Link href="/">
                <a>Login</a>
              </Link>
            </MenuItem>
          </Menu>
        </Flex>
      </Container>
    </Wrapper>
  );
};
export default Navbar;
