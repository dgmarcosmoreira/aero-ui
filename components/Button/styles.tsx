import styled from 'styled-components';
import { theme } from '../../styles/theme';

export const ButtonStyle = styled.a<{variant: 'primary' | 'primaryOutline'}>`
 ${props => props.variant === 'primary' ? theme.buttons.primary : theme.buttons.primaryOutline }
  vertical-align: middle;
`;
