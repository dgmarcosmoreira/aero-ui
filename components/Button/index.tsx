import React from 'react';

import { ButtonStyle } from './styles';

const Button = ({ variant, link, children }) => {
  return (
    <ButtonStyle variant={variant} className={variant} href={link}>
      {children}
    </ButtonStyle>
  );
};

export default Button;
