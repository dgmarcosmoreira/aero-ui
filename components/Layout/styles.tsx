import styled from 'styled-components';
import { theme } from '../../styles/theme';

export const Wrapper = styled.div`
`;

export const Caption = styled.p`
  ${theme.fonts.caption}
  margin-bottom: ${theme.spacers.sm}
`;

export const Title = styled.h1`
  ${theme.fonts.h1}
  margin-bottom: ${theme.spacers.md}
`;

export const Lead = styled.p`
  ${theme.fonts.lead}
  margin-bottom: ${theme.spacers.md}
`;

export const Content = styled.div`
  ${theme.fonts.text}
    ${theme.fonts.base}
`;

export const Separator = styled.div`
  ${theme.fonts.paragraph}
  margin: ${theme.spacers.md} auto;
  border-bottom: 1px solid ${theme.colors.black};
`;

export const Footer = styled.div`
  ${theme.fonts.paragraph}
  margin-bottom: ${theme.spacers.xxl};
`;

export const Buttons = styled.div`
  display: flex;
  gap: 16px;
`;
