import React from 'react';

import { Wrapper, Caption, Title, Lead, Separator, Content, Footer, Buttons } from './styles';
import { Container } from '../../styles/grid';
import { Navbar, Button } from '../index';

const Layout = () => {
  return (
    <Wrapper>
      <Navbar />
      <Container>
        <Caption>LOREM IPSUM</Caption>
        <Title>Lorem ipsum dolor sit amets mauris. </Title>
        <Lead>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce interdum accumsan velit, a varius ipsum
          fringilla eget. Aliquam a augue maximus, facilisis sem sit amet, luctus ex.{' '}
        </Lead>
        <Buttons>
          <Button variant="primary" link="/">Default</Button>
          <Button variant="secondary" link="/">Default</Button>
        </Buttons>
        <Separator />
        <Content>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce interdum accumsan velit, a varius ipsum
          fringilla eget. Aliquam a augue maximus, facilisis sem sit amet, luctus ex. Donec in scelerisque nunc. Nullam
          sed ex semper, rhoncus urna ac, dapibus mauris. Donec eu augue malesuada, volutpat leo in, ullamcorper urna.
          Aliquam at mi sed elit congue mollis. In sollicitudin tellus scelerisque ultricies iaculis. Etiam suscipit
          nulla pharetra ante ullamcorper, at malesuada mi dignissim. Praesent tempor est fringilla, imperdiet magna at,
          gravida justo. Curabitur placerat lectus eu iaculis suscipit. Morbi nec dictum nisl. Etiam at venenatis orci.
          Morbi tristique tellus nec nisl pretium sodales. Donec at nisi a eros varius interdum ut nec turpis. Aenean
          scelerisque lorem purus, at consequat ipsum luctus eget.
        </Content>
        <Footer/>
      </Container>
    </Wrapper>
  );
};
export default Layout;
