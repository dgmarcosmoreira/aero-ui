import styled, { css } from 'styled-components';
import { theme } from './theme';

const breakpoints = ['sm', 'md', 'lg', 'xl'];
export type Breakpoints = 'sm' | 'md' | 'lg' | 'xl';
export type ScreenMap = Partial<Record<Breakpoints, boolean>>;
export type ScreenSizeMap = Partial<Record<Breakpoints, number>>;
export type Gap = number | Partial<Record<Breakpoints, number>>;
export type Cols = number | Partial<Record<Breakpoints, number>>;

export const responsiveArray: Breakpoints[] = ['xl', 'lg', 'md', 'sm'];

export interface RowProps extends React.HTMLAttributes<HTMLDivElement> {
  cols: Cols | [Cols];
  gap?: Gap | [Gap, Gap];
  align?: any;
  justify?: any;
}

export const Breakpoint = {
  smMin: `(min-width: ${theme.grid.sm.breakpoint}px)`,
  mdMin: `(min-width: ${theme.grid.md.breakpoint}px)`,
  lgMin: `(min-width: ${theme.grid.lg.breakpoint}px)`,
  xlMin: `(min-width: ${theme.grid.xl.breakpoint}px)`,
  smMax: `(max-width: ${theme.grid.sm.breakpoint - 0.02}px)`,
  mdMax: `(max-width: ${theme.grid.md.breakpoint - 0.02}px)`,
  lgMax: `(max-width: ${theme.grid.lg.breakpoint - 0.02}px)`,
  xlMax: `(max-width: ${theme.grid.xl.breakpoint - 0.02}px)`,
};


export const Container = styled.div`
  position: relative;
  width: 100%;
  height: 100%;

  margin-right: auto;
  margin-left: auto;

  max-width: ${theme.grid.sm.container}px;

  @media ${Breakpoint.mdMin} {
    max-width: ${theme.grid.md.container}px;
  }
  @media ${Breakpoint.lgMin} {
    max-width: ${theme.grid.lg.container}px;
  }
  @media ${Breakpoint.xlMin} {
    max-width: ${theme.grid.xl.container}px;
  }
`;

const getGap = gap => {
  const horizontal = gap[0];
  const vertical = gap[1];
  let style = '';
  if (vertical instanceof Object) {
    Object.entries(vertical).forEach(entry => {
      const [key, value] = entry;
      breakpoints.forEach(breakpoint => {
        if (breakpoint === key) {
          // todo make breakpoints
          style += `
            row-gap: ${value}px;
          `;
        }
      });
    });
  } else {
    style += `
            row-gap: ${vertical}px;
          `;
  }

  if (horizontal instanceof Object) {
    Object.entries(horizontal).forEach(entry => {
      const [key, value] = entry;
      breakpoints.forEach(breakpoint => {
        // todo make breakpoints
        if (breakpoint === key) {
          style += `
            column-gap: ${value}px;
          `;
        }
      });
    });
  } else {
    style += `
       column-gap: ${horizontal}px;
    `;
  }
  return style;
};
export const Grid = styled.div<RowProps>`
  display: grid;
  grid-template-columns: repeat(${props => (props.cols ? props.cols : 1)}, 1fr);
  gap: 16px;
`;

export const Col = styled.div<RowProps>``;
