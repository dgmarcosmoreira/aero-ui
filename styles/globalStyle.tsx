import { createGlobalStyle } from 'styled-components';
import { theme } from './theme';
import reset from 'styled-reset'

export const GlobalStyle = createGlobalStyle`
  body {
    ${reset}
    color: ${theme.colors.black};
    margin: 0;
    padding: 0;
    ${theme.fonts.base};
    a {
      text-decoration: none;
    }
  }
`;
