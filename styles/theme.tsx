import sharedTheme from './theme.json';

export interface ITheme {
  colors: any;
  buttons: any;
  fonts: any;
  grid: any;
  spacers: any;
}

const getColors = colors => {
  let listColor = {};
  Object.entries(colors).forEach(color=>{
    // @ts-ignore
    listColor[color[0]] = color[1].color;
  });
  return listColor;
};

export const theme: ITheme = {
  fonts: sharedTheme.fonts,
  colors: getColors(sharedTheme.colors),
  grid: sharedTheme.grids,
  spacers: sharedTheme.spacers,
  // @ts-ignore
  buttons: sharedTheme.components.atoms.buttons,
};
