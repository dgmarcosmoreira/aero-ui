// @flow
export type Colors = {
  primary: string;
  secondary: string;
  disabled: string;
}

/** */
export type Theme = {
  colors?: Colors,
}
