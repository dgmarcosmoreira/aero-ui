const fetch = require('node-fetch');
const fs = require('fs');

const figmaId = '0UtiguB3XPek5c6peMLep9';
const figmaApiKey = '61836-0163fdf3-f719-4298-bd53-633c77b7800c';

const rgba2hex = orig => {
  let a,
    isPercent,
    rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
    alpha = ((rgb && rgb[4]) || '').trim(),
    hex = rgb
      ? (rgb[1] | (1 << 8)).toString(16).slice(1) +
        (rgb[2] | (1 << 8)).toString(16).slice(1) +
        (rgb[3] | (1 << 8)).toString(16).slice(1)
      : orig;

  if (alpha !== '') {
    a = alpha;
  } else {
    a = 01;
  }
  // multiply before convert to HEX
  a = ((a * 255) | (1 << 8)).toString(16).slice(1);
  hex = hex + a;

  return hex;
};

const getGrids = foundation => {
  // empty "grids obj" wheree we will store all colors
  const grids = {};
  // get "grids" artboard
  const gridsAtrboard = foundation.filter(item => {
    return item.name === 'grids';
  })[0].children;

  gridsAtrboard.map(item => {
    const gridObj = {
      [item.name]: {
        cols: item.layoutGrids[0].count,
        gap: `${item.layoutGrids[0].gutterSize}`,
        container: `${item.absoluteBoundingBox.width - item.layoutGrids[0].offset * 2}`,
        breakpoint: `${item.absoluteBoundingBox.width}`,
      },
    };

    Object.assign(grids, gridObj);
  });
  return grids;
};

const getSpacers = foundation => {
  // empty "spacers obj" wheree we will store all colors
  const spacers = {};
  // get "spacers" artboard
  const spacersAtrboard = foundation.filter(item => {
    return item.name === 'spacers';
  })[0].children;

  spacersAtrboard.map(item => {
    const spacerObj = {
      [item.name]: `${item.absoluteBoundingBox.height}px`,
    };

    Object.assign(spacers, spacerObj);
  });

  return spacers;
};

const getFontStyles = foundation => {
  // empty "spacers obj" wheree we will store all colors
  const fontStyles = {};
  // get "spacers" artboard
  const fontStylesAtrboard = foundation.filter(item => {
    return item.name === 'typography';
  })[0].children;

  fontStylesAtrboard.map(fontItem => {
    let fontObj;

    if (fontItem.name === 'font-family-base') {
      fontObj = {
        base: {
          id: fontItem.id,
          fontFamily: fontItem.style.fontFamily,
        },
      };
    } else if (fontItem.name === 'font-family-headings') {
      fontObj = {
        headings: {
          id: fontItem.id,
          fontFamily: fontItem.style.fontFamily,
        },
      };
    } else {
      fontObj = {
        [fontItem.name]: {
          id: fontItem.id,
          fontFamily: fontItem.style.fontFamily,
          fontSize: `${fontItem.style.fontSize}px`,
          fontWeight: fontItem.style.fontWeight,
          lineHeight: `${fontItem.style.lineHeightPercent}%`,
          letterSpacing: fontItem.style.letterSpacing !== 0 ? `${fontItem.style.letterSpacing}px` : 'normal',
        },
      };
    }

    Object.assign(fontStyles, fontObj);
  });
  return fontStyles;
};

const getPalette = foundation => {
  // empty "palette obj" wheree we will store all colors
  const palette = {};
  // get "palette" artboard

  const paletteAtrboard = foundation.filter(item => {
    return item.name === 'palette';
  })[0].children;

  // get colors from each children
  paletteAtrboard.map(item => {
    function rbaObj(obj) {
      return Math.round(item.fills[0].color[obj] * 255);
    }

    const color = `rgba(${rbaObj('r')}, ${rbaObj('g')}, ${rbaObj('b')}, ${item.fills[0].color.a})`;
    const hexColor = rgba2hex(color);
    const colorObj = {
      [item.name]: {
        color: `#${hexColor}`,
        id: item.styles && item.styles.fill,
      },
    };

    Object.assign(palette, colorObj);
  });

  return palette;
};

const getColorById = (id, colors) => {
  const palette = getPalette(colors);
  const filter = Object.values(palette)
    .filter(item => {
      return item.id === id;
    })
    .map(item => {
      return item.color;
    });
  return filter[0];
};

const getTextById = (id, text) => {
  const textStyles = getFontStyles(text);
  const filter = Object.values(textStyles).filter(item => {
    return item.id === id;
  });
  return filter[0];
};

const getAtoms = ui => {
  const atoms = {
    buttons: {},
  };

  const buttonsArtboard = ui.atoms.filter(item => {
    return item.name === 'buttons';
  })[0].children;

  buttonsArtboard.map(item => {
    const background = getColorById(item.styles.fills, ui.foundation);
    const strokeColor = getColorById(item.styles.strokes, ui.foundation);
    const child = item.children ? item.children[0].style : '';

    const border = strokeColor ? `${item.strokeWeight}px solid ${strokeColor}` : 'none';

    const buttonsObj = {
      [item.name]: {
        background: background,
        border,
        borderRadius: item.cornerRadius,
        paddingRight: `${item.paddingRight}px`,
        paddingTop: `${item.paddingTop}px`,
        paddingLeft: `${item.paddingLeft}px`,
        paddingBottom: `${item.paddingBottom}px`,
        textAlign: item.children[0].layoutAlign,
        color: getColorById(item.children[0].styles.fill, ui.foundation),
        //fontFamily: child.fontFamily,
        fontWeight: child.fontWeight,
        fontSize: `${child.fontSize}px`,
        lineHeight: `${child.lineHeightPx}px`,
        letterSpacing: child.letterSpacing,
      },
    };

    Object.assign(atoms.buttons, buttonsObj);
  });

  return atoms;
};

const createStyles = async () => {
  const result = await fetch(`https://api.figma.com/v1/files/${figmaId}`, {
    method: 'GET',
    headers: {
      'X-Figma-Token': figmaApiKey,
    },
  });
  const figmaTreeStructure = await result.json();

  const foundation = figmaTreeStructure.document.children.filter(item => {
    return item.name === 'foundations';
  })[0].children;

  const atoms = figmaTreeStructure.document.children.filter(item => {
    return item.name === 'atoms';
  })[0].children;

  const ui = {
    foundation,
    atoms,
  };

  const theme = {
    grids: {},
    spacers: {},
    colors: {},
    fonts: {},
    components: {
      atoms: {},
    },
  };
  Object.assign(theme.grids, getGrids(foundation));
  Object.assign(theme.colors, getPalette(foundation));
  Object.assign(theme.fonts, getFontStyles(foundation));
  Object.assign(theme.spacers, getSpacers(foundation));
  Object.assign(theme.components.atoms, getAtoms(ui));

  fs.writeFile('./styles/theme.json', JSON.stringify(theme), function(err) {
    if (err) throw err;
    console.log('🚀 Process complete. Check your theme.json file');
  });

  return theme;
};

module.exports.styles = () => {
  return createStyles();
};

this.styles();
